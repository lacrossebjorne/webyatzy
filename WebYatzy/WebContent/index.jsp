<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="sv">
<head>
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
<meta charset="UTF-8">
<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/tooltips.js"></script>
<link type="text/css" rel="stylesheet" href="css/tooltips.css" />
<link type="text/css" rel="stylesheet" href="css/buttons.css" />
</head>
<body>
	<div id="header">
		<img src="media/logo.png" />
	</div>
<div id="buttonsDiv">
<script>$("#buttonsDiv").load("buttons.html");</script>
</div>
</body>
<script type="text/javascript">
	$(function(){
		$("#redirectForm:visible").last().css({"border-right" : "1px solid black"});
		$.getScript("js/buttons.js");
	});
</script>
</html>