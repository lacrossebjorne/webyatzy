<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="sv">
<head>
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
<meta charset="UTF-8">
	<link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
<link type="text/css" rel="stylesheet" href="css/buttons.css">
	<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
	<script type="text/javascript" src="js/jquery-ui.min.js"></script>
	<link type="text/css" rel="stylesheet" href="css/tooltips.css">
	<script type="text/javascript" src="js/tooltips.js"></script>
	<title>Yatzy! Highscores</title>
</head>
<body>
	<div id="header">
		<img src="media/logo.png" />
	</div>
	<div class="center">
		<table class="center" id="highscorelist">
			<tbody>
				<tr id="hlisttoprow">
					<td></td>
					<td>Namn</td>
					<td>Poäng</td>
				</tr>
				<c:forEach items="${highscorelist}" var="listItem" varStatus="loop">
				<tr>
					<td>${loop.index + 1}</td>
					<td>${listItem.name}</td>
					<td>${listItem.highscore}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
<div id="bottomButtons">
	<script>$("#bottomButtons").load("buttons.html");</script>
</div>
	</body>
	<script type="text/javascript">
		$(function(){
			$.getScript("js/buttons.js");
		});
	</script>
</html>