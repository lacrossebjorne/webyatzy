<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html>
<head>
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
<meta charset="UTF-8">
<link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>	
<link type="text/css" rel="stylesheet" href="css/tooltips.css" />
<link type="text/css" rel="stylesheet" href="css/buttons.css" />
<script type="text/javascript" src="js/tooltips.js"></script>
<title>Spelarstatistik</title>
</head>
<body>
	<div id="header">
		<img src="media/logo.png" />
	</div>
	<div id="pstats">
		<p>Statistik för ${name} (ID: ${playerID})</p>
	</div>
	<table class="center" style="border: 1px solid white">
		<tr>
			<td>
				<table class="center" id="playerStatsTable">
					<tr id="tableTopRow">
						<th>Spel</th>
						<th>Vinnare</th>
						<th>Poäng</th>
						<th>Tvåa</th>
						<th>Poäng</th>
						<th>Trea</th>
						<th>Poäng</th>
						<th>Fyra</th>
						<th>Poäng</th>
						<th>Femma</th>
						<th>Poäng</th>
						<th>Sexa</th>
						<th>Poäng</th>
						<th>Datum</th>
					</tr>
				</table>
		<div id=overflow>
			<table id="playerStatsTable">
				<tbody id="playerStatsBody">
				</tbody>
			</table>
		</div>
	</table>
<div id="bottomButtons">
	<script>$("#bottomButtons").load("buttons.html");</script>
</div>
	</body>
	<script type="text/javascript">
		$(function(){			
			$.getScript("js/buttons.js");
			
			<c:forEach items = "${playerstats}" var = "listItem">
			var listItem = '${listItem.playerGamesStats}';
			$('#playerStatsBody:last').append('<tr></tr>');
			var columnsArray = listItem.split(",");
			if (columnsArray.length < 14) {
				$.each(columnsArray, function(i, column) {
						$('#playerStatsBody tr:last-child').append('<td>' + column + '</td>');
				});
				for(var j = columnsArray.length; j < 13; j++)
					$('#playerStatsBody tr:last-child').append('<td>–</td>');
				$('#playerStatsBody tr:last-child').append('<td>' + '${listItem.endDate}' + '</td>');
			} else {
				$.each(columnsArray, function(i, column) {
					while (i < 13) {
						$('#playerStatsBody tr:last-child').append('<td>' + column + '</td>');
					}
					$('#playerStatsBody tr:last-child').append('<td>' + '${listItem.endDate}' + '</td>');
				});
			}
			</c:forEach>
		});
	</script>
</body>
</html>