$(function() {
	
	
	//För att hem-knappen inte ska vara synlig på första sidan
	if (window.location.pathname === "/WebYatzy/" || /index/i.test(window.location.pathname)) 
			$("#hiddenhome").css({
			"display" : "none"
			});
	else
		$("#homeButton").css({
			"display" : "block"
		});

	//matar tooltips-pluginen med det den ska visa vid mouse-over på knapparna
	$(".buttons input[title]").tooltips();

	//Tryck esc på tangentbordet stänger menyn
	$(document).keyup(function(e) {
		if (e.keyCode == 27)
			setVisible("");
	});

	//Laddar Highscore-sidan
	$("#highscore").click(function() {
		$("#redirectForm").submit();
	});

	//Tar dig till start-sidan
	$("#homeButton").click(function() {
		window.location = "index.jsp";

	});

	//Vid val i menyn ska inte menyn stängas, klickar du någon annanstans ska menyn stängas
	$(document).on("click", "#hiddengame, #hiddenpstats, #hiddengstats, #hiddenhighscore, #hiddenhome", function(e) {
		if($(this).css({"visibility" : "visible"}))
			e.stopPropagation();
		});

	//Visar menyn Spela. Om den redan är populerad görs inget databas-uppslag. Returnerar false för att requesten inte ska ladda en nya sida.
	$(document).on("click", "#newgame", function() {
		var newGameObj = {"dbQuery" : "Spela", selector : "#newgame"};
			if ($("#playgameselect").find('option').length == 0) {
				getAjax(newGameObj, $("#playgameselect"));
			}
		if ($("#hiddengame").css("visibility") == "hidden") {
			setVisible("#hiddengame, #hiddengame div");
		} else
			setInvisible("#hiddengame, #hiddengame div");
		return false;
	});
	
	//Visar menyn Spelarstatistik. Om den redan är populerad görs inget databas-uppslag. Returnerar false för att requesten inte ska ladda en nya sida.
	$(document).on("click", "#pstats", function() {
		var pstatsObj = {"dbQuery" : "Spelarstatistik", selector : "#pstats"};
		if ($("#pselect").find('option').length == 0) {
			getAjax(pstatsObj, $("#pselect"));
			}
		if ($("#hiddenpstats").css("visibility") == "hidden")
			setVisible("#hiddenpstats, #hiddenpstats div");
		else
			setInvisible("#hiddenpstats, #hiddenpstats div");
		return false;
	});

	//Visar menyn Spelstatistik. Om den redan är populerad görs inget databas-uppslag. Returnerar false för att requesten inte ska ladda en nya sida.
	$(document).on("click", "#gstats", function() {
		var gstatsObj = {"dbQuery" : "Spelstatistik", selector : "#gstats"};
			if ($("#gselect").find('option').length == 0) {
				getAjax(gstatsObj, $("#gselect"));
			}
		if ($("#hiddengstats").css("visibility") == "hidden")
			setVisible("#hiddengstats, #hiddengstats div");
		else
			setInvisible("#hiddengstats, #hiddengstats div");
		return false;
	});

	//Startar nytt spel. Om ett värde finns i newplayer skapas denna. Efter kontroll av antalet spelare starts ett nytt spel.
	$("#playgameselectsubmit").click(function() {
		if($("#newplayer").val() != "") {
			$("#playgameselectform").submit(function(e){
				postNewPlayer($("#newplayer").val());
			});
			e.preventDefault();
		}
		else if ($("#playgameselectform :selected").length > 6) {
			alert("Max 6 spelare kan vara med i ett spel!")
			return false;
		} 
		else if ($("#playgameselectform :selected").length > 0) {
			$("#playgameselectform").attr("action", "GameServlet");
			$("#playgameselectform").submit();
		} else {
			$(".hidden").effect("shake", {distance:3});
			return false;
		}
	});

	//Hämtar statistik för en spelare. Kontroll av att ett giltigt värde matas in i textfältet.
	$("#pselectsubmit").click(function() {
		if ($("#pselect option:selected").val() != null) {
			$("#pselectform").attr("action", "DBqueryServlet");
			$("#pselectform").submit();
		} else {
			var idArray = [];
			$.each($("#pselect option"), function(i, player) {
				var id = $(player).text().split(": ");
				idArray.push(Number(id[0]));
			});
			if ($.inArray(Number($("#pselectInput").val()), idArray) > -1) {
					$("#pselectform").attr("action", "DBqueryServlet");
					$("#pselectform").submit();
				} else {
					$(".hidden").effect("shake", {distance:3});
					return false;
				}
		}
	});

	//Hämtar statistik för ett spel. Kontroll av att ett giltigt värde matas in i textfältet.
	$("#gselectsubmit").click(function() {
		if ($("#gselect option:selected").val() != null) {
			var id = $("#gselect option:selected").val().split(": ");
			$("#gselect option:selected").val(id[0]);
			$("#gselectform").attr("action", "DBqueryServlet");
			$("#gselectform").submit();
		} else {
			var idArray = [];
			$.each($("#gselect option"), function(i, game) {
				var id = $(game).text().split(": ");
				idArray.push(Number(id[0]));
			});
			if ($.inArray(Number($("#gselectInput").val()), idArray) > -1) {
					$("#gselectform").attr("action", "DBqueryServlet");
					$("#gselectform").submit();
				} else {
					$(".hidden").effect("shake", {distance:3});
					return false;
				}
		}
	});

	//klickar man på bakgrunden ska menyerna stängas
	$(document).click(function() {
		setVisible("");
	});
});

//Ajax-request, hämtar data från databasen för presentation i menyn
function getAjax(form, output) {
	return $
			.getJSON({
				url : 'DBqueryServlet',
				data : form,
				success : function(data) {
					if (data.isValid) {
						if (form.selector == "#gstats") {
							$.each(data.allgames, function(index, listItem) {
								output.append('<option>' + listItem.gameID
										+ ': ' + listItem.entryDate
										+ '</option>');
							});
						} else {
							$.each(data.allplayers, function(index, listItem) {
								output.append('<option>' + listItem.playerID
										+ ': ' + listItem.name + '</option>');
							});
						}
					} else
						alert("Ingen kontakt med databasen, kontrollera anslutningarna.");
				}
			});
};

//Ajax-post av ny spelare
function postNewPlayer(newPlayer) {
	return $
			.post({
				url : 'DBqueryServlet',
				data : {
					newplayer : newPlayer
				},
				datatype : 'json',
				success : function(data) {
					if (data.isValid) {
						output.append('<option>' + data.playerID + ': '
								+ data.name + '</option>');
					} else
						alert("Ingen kontakt med databasen, kontrollera anslutningarna.");
				}
			});
};

//Visar vald meny och gömmer övriga. Varje meny består av två divar för att OK-knapp och textfält ska ligga kvar vid scroll.
//Z-index höjs för att menyn ska kunna flyta över andra element (poängprotokollet).
function setVisible(element) {	
	var hideOthers = [ "#hiddengame, #hiddengame div", "#hiddenpstats, #hiddenpstats div", "#hiddengstats, #hiddengstats div" ];
	hideOthers = $.grep(hideOthers, function(n) {
		return (n !== element);
	});
	$.each(hideOthers, function(i, hideelement) {
		setInvisible(hideelement);
	});
	$(element).css({
		"visibility" : "visible",
		"z-index" : "10"
	}).animate({
		opacity : 1.0
	}, 1000);
};

//Göm meny och avaktivera eventuella markeringar.
//z-index sätts till 0 för att andra element inte ska skymmas
function setInvisible(element) {
	$(element).animate({
		opacity : 0.0
	}, 1000);
	$(element).css({
		"visibility" : "hidden",
		"z-index" : "0"
	});
	$("option:selected").prop("selected", false);

};
