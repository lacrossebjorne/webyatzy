var diceSet;
var throwCounter = 1;
var players = [];
var playerList = {};
var newGame;
var order = 1;
var activePlayer;

$(function() {
	// Tärningskast. Skapar ett objekt som skickas till back-end
	$("#throwdice")
			.click(
					function() {
						$("#hiddenP").text(
								"Kast " + throwCounter + " för "
										+ players[order].name);
						if (throwCounter <= 3) {
							if (throwCounter == 3)
								$("#hiddenP")
										.text(
												"Sätt dina poäng genom att klicka i lämplig ruta i spelprotokollet...");
							diceSet = throwDice();
							var playerDiceSetObj = {};
							playerDiceSetObj.player = activePlayer;
							playerDiceSetObj.diceSet = diceSet;
							postPlayerThrow(playerDiceSetObj);
							throwCounter++;
						} else
							$("#hiddenP")
									.text(
											"Var vänlig sätt dina poäng genom att klicka i lämplig ruta i spelprotokollet...");
					});

	// Fångar klick i spelprotokollet, endast de färgade är korrekta
	$("td").click(function() {
		if ($(this).css("background-color") == "rgb(255, 255, 224)") {
			var score = $(this).text();
			resetAnimateScoreSheet($(this));
			resetAnimateDice();
			$(".dice").css('visibility', 'hidden');
			$("#hiddenP").css('visibility', 'hidden');
			$("#hiddenP").text("");
			var playerTurnObj = {};
			playerTurnObj.players = players;
			playerTurnObj.player = activePlayer;
			playerTurnObj.diceSet = diceSet;
			playerTurnObj.score = score;
			playerTurnObj.scoreType = $(this)[0].id;
			postScore(playerTurnObj);
			throwCounter = 1;
		} else if (throwCounter > 1) {
			$("#hiddenP").text("Välj en ledig ruta i spelprotokollet...");
			$("#hiddenP").css({
				"visibility" : "visible"
			});
		}
	});

	// Animerar en tärning som ska sparas
	$(".dice").click(function() {
		animateDice($(this));
	});
});

// Skapar det objekt med spelare som skickas till back-end när ett nytt spel
// startas
function addPlayerToGame(p) {
	players.push(p);
	playerList.players = players;
};

// Spelar objekt
function Player(pid, name) {
	this.pid = pid;
	this.name = name;
};

// Presenterar turordning
function greeting() {
	var string = "";
	order = -1;
	turnCount = 1;
	$.each(players, function(i, player) {
		var playerstring = (i + 1) + ": " + player.name + "\n";
		string += playerstring;
	});
	alert("Dags att spela Yatzy!\nTurordning i spelet:\n" + string);
	playGame();
};

// Håller reda på turordning och när ett spel är färdigspelat. Nollställer
// tärningarna inför en spelares omgång.
// Presenterar vinnare
function playGame() {
	$.each($(".dice"), function(i, die) {
		$(die).attr("value", "");
	});
	order++;
	if (turnCount <= (players.length * 15)) {
		if (order > players.length - 1)
			order = 0;
		$("#hiddenP").text(
				"Kast " + throwCounter + " för " + players[order].name);
		$("#hiddenP").css({
			"visibility" : "visible"
		});
		activePlayer = players[order].pid;
		turnCount++;
	} else {
		var score = 0;
		var winnerID;
		$.each($("*[id=TotSum]"), function(i, sum) {
			if (Number($(sum).text()) > score) {
				score = Number($(sum).text());
				winnerID = $(sum).attr("class");
			}
		});
		var winner = $("#namebracket td." + winnerID).text();
		var saveToDB = confirm("Spelet är slut. Vinnare är " + winner + " med "
				+ score + " poäng!\nSpara spelet till databasen?");
		if (saveToDB)
			postGameToDB();
		else {
			alert("Hoppas du har haft kul, välkommen tillbaka!");
			window.location = "index.jsp";
		}
	}
};

// Återställer en sparad tärning
function resetAnimateDice() {
	$.each($(".dice"), function(i, die) {
		$(die).attr("value", "");
		$(die).css({
			"-webkit-filter" : "invert(0)",
			"filter" : "invert(0)"
		});
	});
};

// Inverterar färgerna på en sparad tärning samt sätter attributet saved
function animateDice(die) {
	if ($(die).attr("value") != "saved") {
		$(die).attr("value", "saved");
		$(die).css({
			"-webkit-filter" : "invert(1)",
			"filter" : "invert(1)"
		});
	} else {
		$(die).attr("value", "");
		$(die).css({
			"-webkit-filter" : "invert(0)",
			"filter" : "invert(0)"
		});
	}
};

// Återstället poängprotokollet efter att poäng sats
function resetAnimateScoreSheet(element) {
	var match = "rgb(255, 255, 224)";
	$.each($("td." + activePlayer), function(i, bracket) {
		if ($(bracket).css("background-color") == match) {
			$(bracket).css({
				"background-color" : "white"
			});
			if ($(bracket)[0] != $(element)[0])
				$(bracket).text("");
		}
	});
};

// Tärningskast
function throwDice() {
	$(".dice").css('visibility', 'visible');

	$.each($(".dice"), function(i, die) {
		if ($(die).attr("value") != "saved")
			$(die).attr("src", randomDie());
	});

	var dice = [];
	var die1value = ($("#die1").attr("src").substring(9, 10));
	var die2value = ($("#die2").attr("src").substring(9, 10));
	var die3value = ($("#die3").attr("src").substring(9, 10));
	var die4value = ($("#die4").attr("src").substring(9, 10));
	var die5value = ($("#die5").attr("src").substring(9, 10));
	dice.push(die1value, die2value, die3value, die4value, die5value);
	return dice;
};

// Random av tärningar
function randomDie() {
	var diePNG = [ 'media/die1.png', 'media/die2.png', 'media/die3.png',
			'media/die4.png', 'media/die5.png', 'media/die6.png' ];
	return diePNG[Math.floor((Math.random() * diePNG.length))];
};

// Ajax-post av ett kast. Animerar poängprotokollet utifrån svar från back-end
function postPlayerThrow(playerDiceSetObj) {
	var jsonDiceSet = JSON.stringify(playerDiceSetObj);
	return $
			.post({
				url : 'GameServlet',
				data : {
					playerThrow : jsonDiceSet
				},
				datatype : 'json',
				success : function(data) {
					if (data.isValid) {
						$.each(data.bracketsAndScore, function(i, bracket) {
							$("td#" + bracket.scoreType + "." + data.player)
									.css({
										"background-color" : "lightyellow"
									});
							$("td#" + bracket.scoreType + "." + data.player)
									.text(bracket.score);
						});
					} else
						alert("Syntax error, något gick fel vid hämtning av spelprotokollet.\nVar vänlig ladda om sidan.")
				}
			});
}

// Ajax-post av vald poäng. Efter svar från back-end sätts övre halvans poäng,
// bonus samt total poäng
function postScore(turnScoreObj) {
	var jsonTurnScore = JSON.stringify(turnScoreObj);
	$
			.post({
				url : 'GameServlet',
				data : {
					playerTurnScore : jsonTurnScore
				},
				datatype : 'json',
				success : function(data) {
					if (data.isValid) {
						$("td#TotSum." + data.player).text(data.sum);
						$("td#SumTop." + data.player).text(data.sumtop);
						$("td#Bonus." + data.player).text(data.bonus);
					} else
						alert("Syntax error, något gick fel vid skrivning tillspelprotokollet.\nVar vänlig ladda om sidan.")
				},
				complete: function() {
					playGame();
				}
			});
};

// Ajax-post om man vill spara till databasen. Sköts helt av back-end
function postGameToDB() {
	$
			.post({
				url : 'GameServlet',
				data : 'saveGameToDB',
				datatype : 'json',
				success : function(data) {
					if (data.saveState)
						$("#hiddenP")
								.text("Spelet har sparats till databasen.");
					else
						$("#hiddenP").text(
								"Det gick inte att spara till databasen...");
					$("#hiddenP").css({
						"visibility" : "visible"
					});
				}
			});
};