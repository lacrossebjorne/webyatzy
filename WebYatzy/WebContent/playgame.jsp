<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="sv">
<head>
<meta charset="UTF-8" />
<link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>	
<link type="text/css" rel="stylesheet" href="css/tooltips.css" />
<link type="text/css" rel="stylesheet" href="css/buttons.css" />
<script type="text/javascript" src="js/tooltips.js"></script>
<link type="text/css" rel="stylesheet" href="css/playgame.css" />
<script src="js/playgame.js"></script>
<link rel="stylesheet" type="text/css"
	href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/ui-lightness/jquery-ui.css" />
	<title>Spela Yatzy!</title>
</head>
<body>
	<div id="header">
		<img src="media/logo.png" />
	</div>
	<div class="scoresheet">
		<table>
			<tbody>
				<tr id="namebracket">
					<td id="bold">Namn</td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td>Ettor</td>
					<td id="Ones"></td>
					<td id="Ones"></td>
					<td id="Ones"></td>
					<td id="Ones"></td>
					<td id="Ones"></td>
					<td id="Ones"></td>
				</tr>
				<tr>
					<td>Tvåor</td>
					<td id="Twos"></td>
					<td id="Twos"></td>
					<td id="Twos"></td>
					<td id="Twos"></td>
					<td id="Twos"></td>
					<td id="Twos"></td>
				</tr>
				<tr>
					<td>Treor</td>
					<td id="Threes"></td>
					<td id="Threes"></td>
					<td id="Threes"></td>
					<td id="Threes"></td>
					<td id="Threes"></td>
					<td id="Threes"></td>
				</tr>
				<tr>
					<td>Fyror</td>
					<td id="Fours"></td>
					<td id="Fours"></td>
					<td id="Fours"></td>
					<td id="Fours"></td>
					<td id="Fours"></td>
					<td id="Fours"></td>
				</tr>
				<tr>
					<td>Femmor</td>
					<td id="Fives"></td>
					<td id="Fives"></td>
					<td id="Fives"></td>
					<td id="Fives"></td>
					<td id="Fives"></td>
					<td id="Fives"></td>
				</tr>
				<tr>
					<td>Sexor</td>
					<td id="Sixes"></td>
					<td id="Sixes"></td>
					<td id="Sixes"></td>
					<td id="Sixes"></td>
					<td id="Sixes"></td>
					<td id="Sixes"></td>
				</tr>
				<tr id="topsumbracket">
					<td id="bold">Summa</td>
					<td id="SumTop"></td>
					<td id="SumTop"></td>
					<td id="SumTop"></td>
					<td id="SumTop"></td>
					<td id="SumTop"></td>
					<td id="SumTop"></td>
				</tr>
				<tr id="bonusbracket">
					<td id="bold">Bonus</td>
					<td id="Bonus"></td>
					<td id="Bonus"></td>
					<td id="Bonus"></td>
					<td id="Bonus"></td>
					<td id="Bonus"></td>
					<td id="Bonus"></td>
				</tr>
				<tr>
					<td>Par</td>
					<td id="Pair"></td>
					<td id="Pair"></td>
					<td id="Pair"></td>
					<td id="Pair"></td>
					<td id="Pair"></td>
					<td id="Pair"></td>
				</tr>
				<tr>
					<td>Tvåpar</td>
					<td id="TwoPairs"></td>
					<td id="TwoPairs"></td>
					<td id="TwoPairs"></td>
					<td id="TwoPairs"></td>
					<td id="TwoPairs"></td>
					<td id="TwoPairs"></td>
				</tr>
				<tr>
					<td>Tretal</td>
					<td id="ThreeOfAKind"></td>
					<td id="ThreeOfAKind"></td>
					<td id="ThreeOfAKind"></td>
					<td id="ThreeOfAKind"></td>
					<td id="ThreeOfAKind"></td>
					<td id="ThreeOfAKind"></td>
				</tr>
				<tr>
					<td>Fyrtal</td>
					<td id="FourOfAKind"></td>
					<td id="FourOfAKind"></td>
					<td id="FourOfAKind"></td>
					<td id="FourOfAKind"></td>
					<td id="FourOfAKind"></td>
					<td id="FourOfAKind"></td>
				</tr>
				<tr>
					<td>Liten</td>
					<td id="SmallStraight"></td>
					<td id="SmallStraight"></td>
					<td id="SmallStraight"></td>
					<td id="SmallStraight"></td>
					<td id="SmallStraight"></td>
					<td id="SmallStraight"></td>
				</tr>
				<tr>
					<td>Stor</td>
					<td id="LargeStraight"></td>
					<td id="LargeStraight"></td>
					<td id="LargeStraight"></td>
					<td id="LargeStraight"></td>
					<td id="LargeStraight"></td>
					<td id="LargeStraight"></td>
				</tr>
				<tr>
					<td>Kåk</td>
					<td id="FullHouse"></td>
					<td id="FullHouse"></td>
					<td id="FullHouse"></td>
					<td id="FullHouse"></td>
					<td id="FullHouse"></td>
					<td id="FullHouse"></td>
				</tr>
				<tr>
					<td>Chans</td>
					<td id="Chance"></td>
					<td id="Chance"></td>
					<td id="Chance"></td>
					<td id="Chance"></td>
					<td id="Chance"></td>
					<td id="Chance"></td>
				</tr>
				<tr>
					<td>Yatzy</td>
					<td id="Yahtzee"></td>
					<td id="Yahtzee"></td>
					<td id="Yahtzee"></td>
					<td id="Yahtzee"></td>
					<td id="Yahtzee"></td>
					<td id="Yahtzee"></td>
				</tr>
				<tr id="totsumbracket">
					<td id="bold">SUMMA</td>
					<td id="TotSum"></td>
					<td id="TotSum"></td>
					<td id="TotSum"></td>
					<td id="TotSum"></td>
					<td id="TotSum"></td>
					<td id="TotSum"></td>
				</tr>
			</tbody>
		</table>
	</div>
	<div id="playingField">
	<div>
		<p id="hiddenP" style="visibility : hidden"></p>
		<div>
			<img class="dice" id="die1" src="media/die1.png">
		</div>
		<div>
			<img class="dice" id="die2" src="media/die6.png"> 
		</div> 
		<div> 
			<img class="dice" id="die3" src="media/die5.png">
		</div>
		<div>
			<img class="dice" id="die4" src="media/die5.png">
		</div>
		<div>
			<img class="dice" id="die5" src="media/die5.png">
		</div>	
	</div>
		<input type="button" class="button" id="throwdice" title="Klicka för att kasta tärningarna" value="Kasta">
	</div>
	<div id="saveddice">
	<hr>
	<p>Klicka på en tärning för att spara den</p>
	</div>
<div id="bottomButtons">
	<script>$("#bottomButtons").load("buttons.html");</script>
</div>
	</body>
	<script type="text/javascript">
		$(function(){
			$.getScript("js/buttons.js");
			var counter = 1;
			<c:forEach items="${playersingame}" var="player">
			var p = new Player('${player.pid}', '${player.name}');
			addPlayerToGame(p);
			$("tbody td:nth-child(" + (counter + 1) + ")").addClass(
					p.pid.toString());
			$("#namebracket ." + p.pid).text(p.name);
			counter++;
			</c:forEach>

			greeting();
			
		});
	</script>
</html>