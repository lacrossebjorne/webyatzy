<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="sv">
<head>
<c:set var="url">${pageContext.request.requestURL}</c:set>
<base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
<meta charset="UTF-8">
<link type="text/css" rel="stylesheet" href="css/stylesheet.css" />
<script type="text/javascript" src="js/jquery-2.2.0.min.js"></script>
<script type="text/javascript" src="js/jquery-ui.min.js"></script>	
<link type="text/css" rel="stylesheet" href="css/tooltips.css" />
<link type="text/css" rel="stylesheet" href="css/buttons.css" />
<script type="text/javascript" src="js/tooltips.js"></script>

<title>Spelstatistik</title>
</head>
<body>
<div id="header">
		<img src="media/logo.png" />
	</div>
	<div id="gstats">
		Statistik över spel ${gameID}
	</div>
	<table class="center" style="border: 1px solid white">
		<tr>
			<td>
				<table class="center" id="gameStatsTable">
					<tr id="tableTopRow">
						<th>Omgång</th>
						<th>Spelare</th>
				 		<th>Poäng typ</th>
						<th>Poäng</th>
						<th>Tärning 1</th>
						<th>Tärning 2</th>
						<th>Tärning 3</th>
						<th>Tärning 4</th>
						<th>Tärning 5</th>
					</tr>
				</table>
		<div id=overflow>
		<table class="center" id="gameStatsTable">
			<tbody id="gameStatsBody">
				<c:forEach items="${gamestats}" var="listItem">
				<tr>
					<td>${listItem.turn}</td>
					<td>${listItem.name}</td>
					<td>${listItem.scoreName}</td>
					<td>${listItem.turnScore}</td>
					<td>${listItem.die1}</td>
					<td>${listItem.die2}</td>
					<td>${listItem.die3}</td>
					<td>${listItem.die4}</td>
					<td>${listItem.die5}</td>
				</tr>
				</c:forEach>
			</tbody>
		</table>
		</div>
	</table>
<div id="bottomButtons">
	<script>$("#bottomButtons").load("buttons.html");</script>
	</div>
</body>
<script type="text/javascript">
		$(function(){
			$.getScript("js/buttons.js");
		});
	</script>
</html>