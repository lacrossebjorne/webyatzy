package com.bd.yatzy.dto;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.bd.yatzy.core.Player;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class PlayerDeserializer implements JsonDeserializer<ArrayList<Player>> {

	@Override
	public ArrayList<Player> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		JsonArray array = json.getAsJsonObject().get("players").getAsJsonArray();
		ArrayList<Player> pList = new ArrayList<>();
		for (JsonElement jsonElement : array) {
			System.out.println(jsonElement.getAsJsonObject());
			Player player = new Player();
			player.setPid(jsonElement.getAsJsonObject().get("pid").getAsInt());
			player.setName(jsonElement.getAsJsonObject().get("name").getAsString());
			pList.add(player);
		}
		return pList;
	}

}
