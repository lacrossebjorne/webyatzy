/*
* DTO för ScoreTypes och poäng
*/
package com.bd.yatzy.dto;

import com.bd.yatzy.core.ScoreType;

public class ScoreTypeAndScoreStore {
	ScoreType scoreType;
	int score;
	
	public ScoreTypeAndScoreStore(ScoreType scoreType, int score) {
		this.scoreType = scoreType;
		this.score = score;
	}

	public ScoreType getScoreType() {
		return scoreType;
	}

	public void setScoreType(ScoreType scoreType) {
		this.scoreType = scoreType;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
		
}
