/*
* JSON-deserializer för en spelares omgång.
*/


package com.bd.yatzy.dto;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.bd.yatzy.core.Dice;
import com.bd.yatzy.core.Die;
import com.bd.yatzy.core.Player;
import com.bd.yatzy.core.ScoreType;
import com.bd.yatzy.core.Turn;
import com.bd.yatzy.servlets.GameServlet;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class TurnDeserializer implements JsonDeserializer<Map<String, Object>> {

	@Override
	public Map<String, Object> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		
		Map<String, Object> map = new HashMap<>();
		
		Dice diceSet = new Dice();
		Die[] dice = diceSet.getDie();
		JsonArray array = json.getAsJsonObject().get("diceSet").getAsJsonArray();
		int i = 0;
		for (JsonElement jsonElement : array) {
			dice[i].setDieValue(jsonElement.getAsInt());
			i++;
		}
		Die[] sortedDiceSet = diceSet.sortDice(dice);
		
		Integer pid = json.getAsJsonObject().get("player").getAsInt();
		ArrayList<Player> pList = GameServlet.getGame().getPlayers();
		Player player = null;
		for (Player p : pList) {
			if (pid  == p.getPid())
				player = p;
		}
		
		String scoreTypeStr = json.getAsJsonObject().get("scoreType").getAsString();
		Integer score = json.getAsJsonObject().get("score").getAsInt();
		checkScoreTypeAndSetScores(player, scoreTypeStr, score, sortedDiceSet);
		
		map.put("player", player.getPid());
		map.put("sum", player.getTotalScore());
		if(player.getTopHalfScore() != 0) {
			map.put("sumtop", player.getTopHalfScore());
			map.put("bonus", player.getBonus());
		}
		
		return map;
	}
	
	private void checkScoreTypeAndSetScores(Player player, String stringType, Integer score, Die[] dice) {
		ScoreType type = null;
		stringType = stringType.toLowerCase();
		switch (stringType) {
		case "ones":
			player.setTopHalfScore(score);
			type = ScoreType.Ones;
			break;
		case "twos":
			player.setTopHalfScore(score);
			type = ScoreType.Twos;
			break;
		case "threes":
			player.setTopHalfScore(score);
			type = ScoreType.Threes;
			break;
		case "fours":
			player.setTopHalfScore(score);
			type = ScoreType.Fours;
			break;
		case "fives":
			player.setTopHalfScore(score);
			type = ScoreType.Fives;
			break;
		case "sixes":
			player.setTopHalfScore(score);
			type = ScoreType.Sixes;
			break;
		case "pair":
			type = ScoreType.Pair;
			break;
		case "twopairs":
			type = ScoreType.TwoPairs;
			break;
		case "threeofakind":
			type = ScoreType.ThreeOfAKind;
			break;
		case "fourofakind":
			type = ScoreType.FourOfAKind;
			break;
		case "smallstraight":
			type = ScoreType.SmallStraight;
			break;
		case "largestraight":
			type = ScoreType.LargeStraight;
			break;
		case "fullhouse":
			type = ScoreType.FullHouse;
			break;
		case "chance":
			type = ScoreType.Chance;
			break;
		case "yahtzee":
			type = ScoreType.Yahtzee;
			break;
		default:
			break;
		}
		player.getScoresheet().put(type, score);
		player.getTurns().add(new Turn(score, dice, type));
		player.setTotalScore(score);
	}
	
}

















