/*
 * DTO for resultat från databasen 
 */

package com.bd.yatzy.dto;


public class DBresultStore {
	
	private String playerID, name, highscore, gameID, endDate, entryDate;
	private String turn, scoreName, turnScore, die1, die2, die3, die4, die5, playerGamesStats;
	
	public String getGameID() {
		return gameID;
	}

	public void setGameID(String gameID) {
		this.gameID = gameID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getHighscore() {
		return highscore;
	}

	public void setHighscore(String highscore) {
		this.highscore = highscore;
	}

	public void setEntryDate(String string) {
		this.entryDate = string;
	}

	public void setEndDate(String string) {
		this.endDate = string;
	}
	
	public String getEntryDate() {
		return entryDate;
	}
	
	public String getEndDate() {
		return endDate;
	}

	public void setPlayerID(String playerID) {
		this.playerID = playerID;
	}
	
	public String getPlayerID() {
		return playerID;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public String getScoreName() {
		return scoreName;
	}

	public void setScoreName(String scoreName) {
		this.scoreName = scoreName;
	}

	public String getTurnScore() {
		return turnScore;
	}

	public void setTurnScore(String turnScore) {
		this.turnScore = turnScore;
	}

	public String getDie1() {
		return die1;
	}

	public void setDie1(String die1) {
		this.die1 = die1;
	}

	public String getDie2() {
		return die2;
	}

	public void setDie2(String die2) {
		this.die2 = die2;
	}

	public String getDie3() {
		return die3;
	}

	public void setDie3(String die3) {
		this.die3 = die3;
	}

	public String getDie4() {
		return die4;
	}

	public void setDie4(String die4) {
		this.die4 = die4;
	}

	public String getDie5() {
		return die5;
	}

	public void setDie5(String die5) {
		this.die5 = die5;
	}

	public String getPlayerGamesStats() {
		return playerGamesStats;
	}

	public void setPlayerGameStats(String playerGamesStats) {
		this.playerGamesStats = playerGamesStats;
	}

}
