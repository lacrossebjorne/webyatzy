/*
* Deserialiserar tärningskast-JSON-objektet som kommer från front-end
* Hämtar aktiv spelare och tärningsvalörerna.
*/


package com.bd.yatzy.dto;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import com.bd.yatzy.core.Dice;
import com.bd.yatzy.core.Die;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

public class ThrowDeserializer implements JsonDeserializer<Map<Integer, Die[]>> {

	@Override
	public Map<Integer, Die[]> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		Integer pid = json.getAsJsonObject().get("player").getAsInt();
		
		Dice diceSet = new Dice();
		Die[] dice = diceSet.getDie();

		JsonArray array = json.getAsJsonObject().get("diceSet").getAsJsonArray();

		int i = 0;
		for (JsonElement jsonElement : array) {
			dice[i].setDieValue(jsonElement.getAsInt());
			i++;
		}
		
		Die[] sortedDiceSet = diceSet.sortDice(dice);		
		
		Map<Integer, Die[]> map = new HashMap<>();
		map.put(pid, sortedDiceSet);

		return map;
	}
}
