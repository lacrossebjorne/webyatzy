package com.bd.yatzy.dto;

import java.lang.reflect.Type;
import java.util.ArrayList;

import com.bd.yatzy.core.Player;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

public class PDeserilizerTest implements JsonDeserializer<ArrayList<Player>> {

	@Override
	public ArrayList<Player> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {

		JsonObject jsonObject = json.getAsJsonObject();
		// System.out.println(jsonObject.toString());
		JsonArray array = jsonObject.get("players").getAsJsonArray();
		System.out.println(array.toString());
		ArrayList<Player> pList = new ArrayList<>();
		for (JsonElement jsonElement : array) {
			System.out.println(jsonElement.getAsJsonObject());
			Player player = new Player();
			player.setPid(jsonElement.getAsJsonObject().get("pid").getAsInt());
			player.setName(jsonElement.getAsJsonObject().get("name").getAsString());
			System.out.println(player.toString());
			pList.add(player);
		}
		return pList;
	}
}
