package com.bd.yatzy.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bd.yatzy.core.DBSaveGame;
import com.bd.yatzy.core.Dice;
import com.bd.yatzy.core.Die;
import com.bd.yatzy.core.Game;
import com.bd.yatzy.core.Player;
import com.bd.yatzy.core.Turn;
import com.bd.yatzy.dto.ThrowDeserializer;
import com.bd.yatzy.dto.ScoreTypeAndScoreStore;
import com.bd.yatzy.dto.TurnDeserializer;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Servlet implementation class GameServlet
 */
@WebServlet("/GameServlet")
public class GameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Game game;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		request.setCharacterEncoding("UTF-8");
		final GsonBuilder builder = new GsonBuilder();
		builder.registerTypeAdapter(Dice.class, new ThrowDeserializer());
		builder.registerTypeAdapter(Turn.class, new TurnDeserializer());
		final Gson gson = builder.create();
		
		if (request.getParameter("playersingame") != null) {
			String[] players = request.getParameterValues("playersingame");
			ArrayList<Player> pList = new ArrayList<>();
			for (String string : players) {
				String[] str = string.split(": ");
				int pid = Integer.parseInt(str[0]);
				String name = str[1];
				Player player = new Player(pid, name);
				pList.add(player);
			}
			game = new Game();
			game.setPlayers(pList);
			request.setAttribute("newGame", game);
			request.setAttribute("playersingame", pList);
			RequestDispatcher dispatcher = request.getRequestDispatcher("playgame.jsp");
			dispatcher.forward(request, response);
		}
		
		if (request.getParameter("playerThrow") != null) {
			Map<Integer, Die[]> map = gson.fromJson(request.getParameter("playerThrow"), Dice.class);
			ArrayList<Player> pList = game.getPlayers();
			Player player = null;
			for (Player p : pList) {
				if (map.containsKey(p.getPid()))
					player = p;
			}
			ArrayList<ScoreTypeAndScoreStore> bracketAndScore = game.playerTurn(player, map.get(player.getPid()));
			Map<String, Object> responseMap = new HashMap<>();
			boolean isValid = false;
			if (!bracketAndScore.isEmpty()) {
				responseMap.put("bracketsAndScore", bracketAndScore);
				responseMap.put("player", player.getPid());
				isValid = true;
			}
			responseMap.put("isValid", isValid);
			write(response, responseMap);
		}
		
		else if (request.getParameter("playerTurnScore") != null) {
			Map<String, Object> map = gson.fromJson(request.getParameter("playerTurnScore"), Turn.class);
			boolean isValid = false;
			if(!map.isEmpty())
				isValid = true;
			map.put("isValid", isValid);
			write(response, map);
		}
		
		else if (request.getParameter("saveGameToDB") != null) {
			Map<String, Object> map = new HashMap<>();
			boolean saveState = DBSaveGame.insertGameData(game.getPlayers());
			map.put("saveState", saveState);
			write(response, map);
		}
	}


	private void write(HttpServletResponse response, Map<String, Object> responseMap) {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(new Gson().toJson(responseMap));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Game getGame() {
		return game;
	}

}
