package com.bd.yatzy.servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.bd.yatzy.core.DBQuery;
import com.bd.yatzy.dto.DBresultStore;
import com.google.gson.Gson;

@WebServlet("/DBqueryServlet")
public class DBqueryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String sqlQuery = request.getParameter("dbQuery");
		DBQuery dbQuery = new DBQuery();

		if (sqlQuery.equals("Highscore")) {
			response.setContentType("text/html");
			ArrayList<DBresultStore> list = dbQuery.getHighScoreList();
			request.setAttribute("highscorelist", list);
			RequestDispatcher dispatcher = request.getRequestDispatcher("highscore.jsp");
			dispatcher.forward(request, response);

		} else if (sqlQuery.equals("Spela")) {
			Map<String, Object> map = new HashMap<String, Object>();
			ArrayList<DBresultStore> list = dbQuery.getAllPlayers();
			boolean isValid = false;
			if (!list.isEmpty()) {
				map.put("allplayers", list);
				isValid = true;
			}
			map.put("isValid", isValid);
			write(response, map);

		} else if (sqlQuery.equals("Spelarstatistik")) {
			Map<String, Object> map = new HashMap<String, Object>();
			ArrayList<DBresultStore> list = dbQuery.getPlayersInGames();
			boolean isValid = false;
			if (!list.isEmpty()) {
				map.put("allplayers", list);
				isValid = true;
			}
			map.put("isValid", isValid);
			write(response, map);

		} else if (sqlQuery.equals("Spelstatistik")) {
			Map<String, Object> map = new HashMap<String, Object>();
			ArrayList<DBresultStore> list = dbQuery.getAllGames();
			boolean isValid = false;
			if (!list.isEmpty()) {
				map.put("allgames", list);
				isValid = true;
			}
			map.put("isValid", isValid);
			write(response, map);
		}
	}

	private void write(HttpServletResponse response, Map<String, Object> map) {
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		try {
			response.getWriter().write(new Gson().toJson(map));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		request.setCharacterEncoding("UTF-8");
		DBQuery dbQuery = new DBQuery();

		if (request.getParameter("getPlayerStats") != null) {
			String player = request.getParameter("getPlayerStats");
			String playerID = "";
			String name = "";
			if (player.contains(":")) {
				playerID = player.split(": ")[0];
				name = player.split(": ")[1];
			} else {
				playerID = player;
				name = dbQuery.getPlayerName(playerID);
			}
			ArrayList<DBresultStore> list = dbQuery.getPlayerStats(playerID);
			System.out.println(list.isEmpty());
			if (!list.isEmpty()) {
				request.setAttribute("name", name);
				request.setAttribute("playerID", playerID);
				request.setAttribute("playerstats", list);
				RequestDispatcher dispatcher = request.getRequestDispatcher("playerstats.jsp");
				dispatcher.forward(request, response);
			}

		} else if (request.getParameter("getGameStats") != null) {
			String gameID = request.getParameter("getGameStats").trim();
			ArrayList<DBresultStore> list = dbQuery.getGameHistory(gameID);
			request.setAttribute("gameID", gameID);
			request.setAttribute("gamestats", list);
			RequestDispatcher dispatcher = request.getRequestDispatcher("gamestats.jsp");
			dispatcher.forward(request, response);

		} else if (request.getParameter("newplayer") != null) {
			String newPlayer = request.getParameter("newplayer");
			Map<String, Object> map = dbQuery.insertPlayer(newPlayer);
			boolean isValid = false;
			if (!map.isEmpty())
				isValid = true;
			map.put("isValid", isValid);
			write(response, map);
		}
	}
}
