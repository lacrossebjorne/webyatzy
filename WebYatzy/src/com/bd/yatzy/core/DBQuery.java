/*
 *Klass för databas-uppslag
 */


package com.bd.yatzy.core;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.bd.yatzy.dto.DBresultStore;

public class DBQuery {
	PreparedStatement statement;
	ResultSet result;
	DBConnect connect;

	public ArrayList<DBresultStore> getGameHistory(String gameID) {
		String sqlQuery = "EXEC [dbo].[uspGetAllTurnsFromGameID] " + gameID;
		ResultSet result = getSQLQueryResult(sqlQuery);
		ArrayList<DBresultStore> list = new ArrayList<>();
		DBresultStore store = null;
		try {
			while (result.next()) {
				store = new DBresultStore();
				store.setTurn(result.getString("Turn"));
				store.setName(result.getString("Player"));
				store.setScoreName(result.getString("ScoreName"));
				store.setTurnScore(result.getString("Points"));
				store.setDie1(result.getString("Die1"));
				store.setDie2(result.getString("Die2"));
				store.setDie3(result.getString("Die3"));
				store.setDie4(result.getString("Die4"));
				store.setDie5(result.getString("Die5"));
				list.add(store);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				result.close();
				closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<DBresultStore> getAllPlayers() {
		String sqlQuery = "SELECT * FROM [dbo].[Players] ORDER BY PlayerID";
		ResultSet result = getSQLQueryResult(sqlQuery);
		ArrayList<DBresultStore> list = new ArrayList<>();
		DBresultStore store = null;
		try {
			while (result.next()) {
				store = new DBresultStore();
				store.setPlayerID(result.getString("PlayerID"));
				store.setName(result.getString("Name"));
				list.add(store);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				result.close();
				closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public String getPlayerName(String playerID) {
		String sqlQuery = "SELECT Name FROM [dbo].[Players] WHERE PlayerID = " + playerID;
		ResultSet result = getSQLQueryResult(sqlQuery);
		String name = "";
		try {
			while (result.next())
				name = result.getString("Name");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return name;
	}

	public ArrayList<DBresultStore> getPlayersInGames() {
		String sqlQuery = "SELECT p.PlayerID, p.Name FROM [dbo].[Players] as p JOIN [dbo].[Players_Games] as pg ON p.PlayerID = pg.Player_ID GROUP BY Name, PlayerID";
		ResultSet result = getSQLQueryResult(sqlQuery);
		ArrayList<DBresultStore> list = new ArrayList<>();
		DBresultStore store = null;
		try {
			while (result.next()) {
				store = new DBresultStore();
				store.setPlayerID(result.getString("PlayerID"));
				store.setName(result.getString("Name"));
				list.add(store);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				result.close();
				closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<DBresultStore> getAllGames() {
		String sqlQuery = "SELECT * FROM [dbo].[Games] ORDER BY GameID";
		ResultSet result = getSQLQueryResult(sqlQuery);
		ArrayList<DBresultStore> list = new ArrayList<>();
		DBresultStore store = null;
		try {
			while (result.next()) {
				store = new DBresultStore();
				store.setGameID(result.getString("GameID"));
				store.setEntryDate(result.getString("EntryDate"));
				store.setEndDate(result.getString("EndDate"));
				list.add(store);
			}
			return list;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				result.close();
				closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<DBresultStore> getHighScoreList() {
		String sqlQuery = "SELECT * FROM [dbo].[vHighScore]";
		ResultSet result = getSQLQueryResult(sqlQuery);
		ArrayList<DBresultStore> list = new ArrayList<>();
		DBresultStore store = null;
		try {
			while (result.next()) {
				store = new DBresultStore();
				store.setName(result.getString("Name"));
				store.setHighscore(result.getString("Highscore"));
				list.add(store);
			}
			return list;
		} catch (SQLException e) {
			System.out.println("SQL query failed.");
			e.printStackTrace();
			return null;
		} finally {
			try {
				result.close();
				closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public ArrayList<DBresultStore> getPlayerStats(String playerID) {
		String sqlQuery = "exec dbo.uspGetPlayerStats " + playerID;
		ResultSet result = getSQLQueryResult(sqlQuery);
		ArrayList<DBresultStore> list = new ArrayList<>();
		DBresultStore store = null;
		try {
			String gameID = "";
			String score;
			String name;
			String date = "";
			String toStore = "";
			while (result.next()) {
				if (!gameID.equals(result.getString("Game_ID"))) {
					if (toStore.length() > 0) {
						store.setEndDate(date);
						store.setPlayerGameStats(toStore);
						list.add(store);
						toStore = "";
					}
					store = new DBresultStore();
					gameID = result.getString("Game_ID");
					name = result.getString("Name");
					score = result.getString("FinalScore");
					date = result.getString("EndDate");
					toStore = gameID + "," + name + "," + score;
				} else {
					name = result.getString("Name");
					score = result.getString("FinalScore");
					toStore = toStore + "," + name + "," + score;
				}
				store.setEndDate(date);
				store.setPlayerGameStats(toStore);
			}
			list.add(store);
			return list;
		} catch (SQLException e) {
			System.out.println("SQL query failed.");
			e.printStackTrace();
			return null;
		} finally {
			try {
				result.close();
				closeConnection();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	public Map<String, Object> insertPlayer(String name) {
		connect = new DBConnect();
		connect.Connect();
		Map<String, Object> map = new HashMap<>();
		CallableStatement callInsertPlayer = null;
		try {
			callInsertPlayer = connect.getConnection().prepareCall("{call dbo.uspInsertPlayer(?, ?)}");
			callInsertPlayer.setString(1, name);
			callInsertPlayer.registerOutParameter(1, Types.VARCHAR);
			callInsertPlayer.registerOutParameter(2, Types.INTEGER);
			callInsertPlayer.execute();
			map.put("playerID", callInsertPlayer.getInt("PlayerID"));
			map.put("playerName", callInsertPlayer.getString("Name"));
			callInsertPlayer.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				callInsertPlayer.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			connect.Disconnect();
		}
		return map;
	}

	public ResultSet getSQLQueryResult(String sqlQuery) {
		statement = null;
		result = null;
		connect = new DBConnect();
		connect.Connect();
		try {
			statement = connect.getConnection().prepareStatement(sqlQuery);
			result = statement.executeQuery();
			return result;
		} catch (SQLException sqle) {
			sqle.printStackTrace();
			return null;
		}
	}

	public void closeConnection() {
		try {
			statement.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		connect.Disconnect();
	}

	public void printResult(ResultSet result) {
		try {
			ResultSetMetaData metaData = result.getMetaData();
			int columnCount = metaData.getColumnCount();

			for (int column = 1; column <= columnCount; column++) {
				String columnName = metaData.getColumnLabel(column);
				System.out.printf("%-15s", columnName == null ? "NULL" : columnName);
			}
			System.out.printf("%n");
			while (result.next()) {
				for (int column = 1; column <= columnCount; column++) {
					Object columnData = result.getObject(column);
					System.out.printf("%-15s", columnData == null ? "NULL" : columnData);
				}
				System.out.printf("\n");
			}
			System.out.println("\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
