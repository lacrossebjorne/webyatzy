package com.bd.yatzy.core;

public class Dice {
	private Die[] dice;

	public Dice() {
		dice = new Die[5];
		for (int i = 0; i < dice.length; i++)
			dice[i] = new Die();
		setDie(dice);
	}

	public Die[] sortDice(Die[] diceToSort) {
		int out, in, min;
		for (out = 0; out < diceToSort.length - 1; out++) {
			min = out;
			for (in = out + 1; in < diceToSort.length; in++)
				if (diceToSort[in].getDieValue() < diceToSort[min].getDieValue())
					min = in;
			Die temp = diceToSort[min];
			diceToSort[min] = diceToSort[out];
			diceToSort[out] = temp;
		}
		return diceToSort;
	}

	public Die[] getDie() {
		return dice;
	}

	public void setDie(Die[] dice) {
		this.dice = dice;
	}
}
