/*
 * Klass för att spara ett spel till databasen. Hämtar respektive spelares scoresheet och Turns 
 */


package com.bd.yatzy.core;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class DBSaveGame {

	public static boolean insertGameData(ArrayList<Player> players) {

		DBConnect dbConnect = new DBConnect();
		dbConnect.Connect();
		Connection connection = dbConnect.getConnection();

		CallableStatement statement = null;

		try {
			connection.setAutoCommit(false);

			try {
				// Creates new Game entry in table Games, returns GameID
				statement = connection.prepareCall("{call dbo.uspNewGame(?, ?)}");
				LocalDate date = LocalDate.now();
				String endDate = date.format(DateTimeFormatter.BASIC_ISO_DATE);
				statement.setString("EndDate", endDate);
				statement.registerOutParameter("GameID", Types.INTEGER);
				statement.execute();
				int gameID = statement.getInt("GameID");
				System.out.println("GameID: " + gameID);

				statement.clearParameters();

				// Insert player data into the DB
				for (Player p : players) {

					// New PlayerGames entry in Table Players_Games, returns a
					// PlayerGameID
					statement = connection.prepareCall("{call dbo.uspNewPlayerGamesEntry(?, ?, ?, ?, ?, ?)}");
					int pid = p.getPid();
					statement.setInt(1, gameID);
					statement.setInt(2, pid);
					statement.setInt(3, p.getTotalScore());
					statement.setInt(4, p.getTopHalfScore());
					statement.setInt(5, p.getBonus());
					statement.registerOutParameter(6, Types.INTEGER);
					statement.execute();
					int pgID = statement.getInt(6);
					System.out.println("PlayerGamesID: " + pgID);
					statement.clearParameters();

					// Enter Turns data and DiceCombos
					// Sets the die values in Dice table for this turn. Returns
					// DiceID
					int turnIndex = 1;
					for (Turn turn : p.getTurns()) {
						statement = connection.prepareCall("{call dbo.uspInsertDiceCombo(?, ?, ?, ?, ?, ?)}");
						Die[] dice = turn.getDice();
						statement.setInt(1, dice[0].getDieValue());
						statement.setInt(2, dice[1].getDieValue());
						statement.setInt(3, dice[2].getDieValue());
						statement.setInt(4, dice[3].getDieValue());
						statement.setInt(5, dice[4].getDieValue());
						statement.registerOutParameter(6, Types.INTEGER);
						statement.execute();
						int dcID = statement.getInt(6);
						System.out.println("DiceComboID: " + dcID);

						statement.close();

						// Creates a new turn entry in table Turn.
						statement = connection.prepareCall("{call dbo.uspInsertTurn(?, ?, ?, ?, ?)}");
						statement.setInt(1, turnIndex++);
						statement.setInt(2, turn.getScoreType().index());
						statement.setInt(3, turn.getScore());
						statement.setInt(4, dcID);
						statement.setInt(5, pgID);
						statement.execute();
					}
				}
				connection.commit();
				return true;
			} catch (SQLException e) {
				e.printStackTrace();
				connection.rollback();
				return false;
			} finally {
				if (statement != null)
					statement.close();
				connection.setAutoCommit(true);
				if (connection != null)
					connection.close();
			}
		} catch (SQLException e1) {
			e1.printStackTrace();
			return false;
		}
		
	}
}
