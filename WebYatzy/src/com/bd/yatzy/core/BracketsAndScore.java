/*
 * Denna klass sköter beräkningar av poäng för respektive poängalternativ
 */



package com.bd.yatzy.core;

import java.util.Arrays;

public class BracketsAndScore {

	public static int getBracketsAndScore(Die[] dice, ScoreType bracket) {
		int sum = 0;
		switch (bracket) {
		case Ones:
			for (Die d : dice) {
				if (d.getDieValue() == 1)
					sum += d.getDieValue();
			}
			break;
		case Twos:
			for (Die d : dice) {
				if (d.getDieValue() == 2)
					sum += d.getDieValue();
			}
			break;
		case Threes:
			for (Die d : dice) {
				if (d.getDieValue() == 3)
					sum += d.getDieValue();
			}
			break;
		case Fours:
			for (Die d : dice) {
				if (d.getDieValue() == 4)
					sum += d.getDieValue();
			}
			break;
		case Fives:
			for (Die d : dice) {
				if (d.getDieValue() == 5)
					sum += d.getDieValue();
			}
			break;
		case Sixes:
			for (Die d : dice) {
				if (d.getDieValue() == 6)
					sum += d.getDieValue();
			}
			break;
		case Pair:
			
			for (int i = 0; i < 4; i++) {
				if (dice[i].getDieValue() == dice[i + 1].getDieValue())
					sum = dice[i].getDieValue() * 2;
			}
			break;
		case TwoPairs:
			for (int i = 0; i < 2; i++) {
				if (dice[i].getDieValue() == dice[i + 1].getDieValue()
						&& dice[i + 2].getDieValue() == dice[i + 3].getDieValue()
						&& dice[i].getDieValue() != dice[i + 2].getDieValue())
					sum = dice[i].getDieValue() * 2 + dice[i + 2].getDieValue() * 2;
				else if (dice[0].getDieValue() == dice[1].getDieValue()
						&& dice[3].getDieValue() == dice[4].getDieValue()
						&& dice[0].getDieValue() != dice[3].getDieValue())
					sum = dice[0].getDieValue() * 2 + dice[3].getDieValue() * 2;
			}
			break;
		case ThreeOfAKind:
			for (int i = 0; i < 3; i++) {
				if (dice[i].getDieValue() == dice[i + 1].getDieValue()
						&& dice[i].getDieValue() == dice[i + 2].getDieValue())
					sum = dice[i].getDieValue() * 3;
			}
			break;
		case FourOfAKind:
			for (int i = 0; i < 2; i++) {
				if (dice[i].getDieValue() == dice[i + 1].getDieValue()
						&& dice[i].getDieValue() == dice[i + 2].getDieValue()
						&& dice[i].getDieValue() == dice[i + 3].getDieValue())
					sum = dice[i].getDieValue() * 4;
			}
			break;
		case SmallStraight:
			if (dice[0].getDieValue() == 1 && dice[1].getDieValue() == 2 && dice[2].getDieValue() == 3
					&& dice[3].getDieValue() == 4 && dice[4].getDieValue() == 5) {
				sum = 15;
			}
			break;
		case LargeStraight:
			if (dice[0].getDieValue() == 2 && dice[1].getDieValue() == 3 && dice[2].getDieValue() == 4
					&& dice[3].getDieValue() == 5 && dice[4].getDieValue() == 6) {
				sum = 20;
			}
			break;
		case FullHouse: {
			if ((dice[0].getDieValue() == dice[1].getDieValue()) && (dice[2].getDieValue() == dice[3].getDieValue()
					&& dice[2].getDieValue() == dice[4].getDieValue()) && dice[0].getDieValue() != dice[4].getDieValue()) {
				sum = (dice[0].getDieValue() * 2) + (dice[2].getDieValue() * 3);
			} else if ((dice[0].getDieValue() == dice[1].getDieValue()
					&& dice[0].getDieValue() == dice[2].getDieValue())
					&& (dice[3].getDieValue() == dice[4].getDieValue()) && dice[0].getDieValue() != dice[4].getDieValue()) {
				sum = (dice[0].getDieValue() * 3) + (dice[3].getDieValue() * 2);
			}
		}
			break;
		case Chance:
			for (Die aDice : dice) {
				sum += aDice.getDieValue();
			}
			break;
		case Yahtzee:
			if (Arrays.stream(dice).allMatch(die -> die.getDieValue() == (dice[0].getDieValue())) == true) {
				sum = 50;
			}
			break;
		default:
			break;
		}
		return sum;
	}

}
