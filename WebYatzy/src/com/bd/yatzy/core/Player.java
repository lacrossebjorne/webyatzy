package com.bd.yatzy.core;

import java.util.ArrayList;
import java.util.HashMap;

public class Player {
	private String name;
	private int pid;
	private ArrayList<Turn> turns;
	private HashMap<ScoreType, Integer> scoresheet;
	boolean isBonusSet = false;

	public Player() {
		setTurns(new ArrayList<>());
		setScoresheet(new HashMap<>());
	}

	public Player(int pid, String name) {
		this.name = name;
		this.pid = pid;
		setTurns(new ArrayList<>());
		setScoresheet(new HashMap<>());
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPid() {
		return pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public ArrayList<Turn> getTurns() {
		return turns;
	}

	public void setTurns(ArrayList<Turn> turns) {
		this.turns = turns;
	}

	public int getTopHalfScore() {
		if(scoresheet.get(ScoreType.SumTop) != null)
			return scoresheet.get(ScoreType.SumTop);
		else
			return 0;
	}

	public void setTopHalfScore(int entry) {
		if (scoresheet.get(ScoreType.SumTop) != null)
			scoresheet.put(ScoreType.SumTop, (getTopHalfScore() + entry));
		else
			scoresheet.put(ScoreType.SumTop, entry);
		setBonus();
	}

	public int getTotalScore() {
		return scoresheet.get(ScoreType.TotSum);
	}

	public void setTotalScore(int entry) {
		if (scoresheet.get(ScoreType.TotSum) != null)
			scoresheet.put(ScoreType.TotSum, (getTotalScore() + entry));
		else
			scoresheet.put(ScoreType.TotSum, entry);
	}

	public int getBonus() {
		return scoresheet.get(ScoreType.Bonus);
	}

	public void setBonus() {
		if(!isBonusSet)
			if (getTopHalfScore() >= 63) {
				scoresheet.put(ScoreType.Bonus, 50);
				scoresheet.put(ScoreType.TotSum, (getTotalScore() + 50));
				isBonusSet = true;
			} else
				scoresheet.put(ScoreType.Bonus, 0);
	}

	public HashMap<ScoreType, Integer> getScoresheet() {
		return scoresheet;
	}

	public void setScoresheet(HashMap<ScoreType, Integer> scoresheet) {
		this.scoresheet = scoresheet;
	}

	public boolean isScoresheetFull() {
		if (getTurns().size() < 15)
			return false;
		else
			return true;
	}

	@Override
	public String toString() {
		return getPid() + ": " + getName() + ": " + (getTurns() != null ? getTurns() : "null") + ": "
				+ (getScoresheet() != null ? getScoresheet() : "null");

	}
}
