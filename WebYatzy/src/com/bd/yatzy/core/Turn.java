package com.bd.yatzy.core;


public class Turn {
	
	private int score;
	private Die[] dice;
	private ScoreType scoreType;
	
	public Turn(int score, Die[] dice, ScoreType type) {
		setScore(score);
		setDice(dice);
		setScoreType(type);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public Die[] getDice() {
		return dice;
	}

	public void setDice(Die[] dice) {
		this.dice = dice;
	}

	public void setScoreType(ScoreType scoreType) {
		this.scoreType = scoreType;
	}
	
	public ScoreType getScoreType() {
		return scoreType;
	}
}
