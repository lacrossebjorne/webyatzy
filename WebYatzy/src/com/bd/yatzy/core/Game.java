package com.bd.yatzy.core;

import java.util.ArrayList;


import com.bd.yatzy.dto.ScoreTypeAndScoreStore;

public class Game {

	private ArrayList<Player> players;

	public Game() {

	}
	
	
	//Kontrollerar lediga rutor i protokollet och hämtar poäng för dessa.
	public ArrayList<ScoreTypeAndScoreStore> playerTurn(Player player, Die[] diceSet) {
		ArrayList<ScoreTypeAndScoreStore> store = new ArrayList<>();
		for (ScoreType y : ScoreType.values()) {
			if (player.getScoresheet().containsKey(y) || y == ScoreType.Bonus || y == ScoreType.SumTop
					|| y == ScoreType.TotSum)
				continue;
			else {
				int calcScore = BracketsAndScore.getBracketsAndScore(diceSet, y);
				store.add(new ScoreTypeAndScoreStore(y, calcScore));
			}
		}
		return store;
	}

	//Sätter poäng för en spelare utifrån val som gjorts i front-end
	public void setScore(Player player, ScoreType scoreType, int score, Die[] dice) {
		player.getScoresheet().put(scoreType, score);
		player.getTurns().add(new Turn(score, dice, scoreType));
		player.setTopHalfScore(score);
		player.setTotalScore(score);
	}

	public ArrayList<Player> getPlayers() {
		return players;
	}

	public void setPlayers(ArrayList<Player> players) {
		this.players = players;
	}
}