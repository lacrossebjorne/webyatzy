package com.bd.yatzy.core;

import java.sql.Connection;
import java.sql.DriverManager;

public class DBConnect {
	private static final String HOST = "jdbc:sqlserver://172.16.41.129:1433;DatabaseName=WebYatzyDB";
	private Connection connection = null;
	private String dbuser = "sa";
	private String password = "Password123";
	private boolean connectionStatus = false;

	public void Connect() {
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			setConnection(DriverManager.getConnection(HOST, dbuser, password));
			setConnectionStatus(true);
		} catch (Exception e) {
			setConnectionStatus(false);
			e.printStackTrace();
		}
	}
	
	public boolean Disconnect() {
		try {
			getConnection().close();
			setConnectionStatus(false);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	public boolean isConnectionStatus() {
		return connectionStatus;
	}

	public void setConnectionStatus(boolean connectionStatus) {
		this.connectionStatus = connectionStatus;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
}
