package com.bd.yatzy.core;

public enum ScoreType {
	Ones(1), Twos(2), Threes(3), Fours(4), Fives(5), Sixes(6), SumTop(7), Bonus(8), Pair(9), TwoPairs(10), ThreeOfAKind(11), FourOfAKind(12), 
	SmallStraight(13), LargeStraight(14), FullHouse(15), Chance(16), Yahtzee(17), TotSum(18);
	

	private int index;

	ScoreType (int index) {
		this.index = index;
	}
	
	public int index() {
		return index;
	}
}
