create table Games
(
	GameID int primary key not null identity(1,1),
	EntryDate date not null DEFAULT getdate() CONSTRAINT ck_entrydate_less_or_eq_today CHECK (EntryDate<=getdate()),
	EndDate date CONSTRAINT ck_enddate_less_or_eq_entrydate CHECK (EndDate<=getdate())
)

create table Players
(
	PlayerID int primary key not null identity(1,1),
	Name varchar(20) not null CONSTRAINT ck_name_length CHECK (LEN(Name)>0)
)

create table Players_Games
(
	PlayerGameID int primary key not null identity(1,1),
	Game_ID int not null,
	Player_ID int not null,
	FinalScore int,
	TopHalfScore int,
	Bonus int not null DEFAULT 0,
	CONSTRAINT fk_PlayerGames_GamesID foreign key(Game_ID) references Games(GameID),
	CONSTRAINT fk_PlayerGames_PlayerID foreign key(Player_ID) references Players(PlayerID)
)

create table ScoreNames
(
	ScoreNameID int primary key not null identity(1,1),
	ScoreName varchar(20) not null 
)

create table DiceCombo
(
	DiceComboID int primary key not null identity(1,1),
	Die1 int not null CONSTRAINT ck_die1_value CHECK (Die1 BETWEEN 1 AND 6),
	Die2 int not null CONSTRAINT ck_die2_value CHECK (Die2 BETWEEN 1 AND 6),
	Die3 int not null CONSTRAINT ck_die3_value CHECK (Die3 BETWEEN 1 AND 6),
	Die4 int not null CONSTRAINT ck_die4_value CHECK (Die4 BETWEEN 1 AND 6),
	Die5 int not null CONSTRAINT ck_die5_value CHECK (Die5 BETWEEN 1 AND 6)
)

create table Turns
(
	TurnID int not null primary key identity(1,1),
	Turn int not null,
	ScoreName_ID int not null,
	TurnScore int not null,
	DiceCombo_ID int not null,
	PlayerGame_ID int not null,
	CONSTRAINT fk_Turns_ScoreNameID foreign key(ScoreName_ID) references ScoreNames(ScoreNameID),
	CONSTRAINT fk_Turns_DiceComboID foreign key(DiceCombo_ID) references DiceCombo(DiceComboID),
	CONSTRAINT fk_Turns_PlayerGameID foreign key(PlayerGame_ID) references Players_Games(PlayerGameID)
)

insert into ScoreNames(ScoreName) values ('Ettor')
insert into ScoreNames(ScoreName) values ('Tvåor')
insert into ScoreNames(ScoreName) values ('Treor')
insert into ScoreNames(ScoreName) values ('Fyror')
insert into ScoreNames(ScoreName) values ('Femmor')
insert into ScoreNames(ScoreName) values ('Sexor')
insert into ScoreNames(ScoreName) values ('Summa Övre')
insert into ScoreNames(ScoreName) values ('Bonus')
insert into ScoreNames(ScoreName) values ('Par')
insert into ScoreNames(ScoreName) values ('Två Par')
insert into ScoreNames(ScoreName) values ('Tretal')
insert into ScoreNames(ScoreName) values ('Fyrtal')
insert into ScoreNames(ScoreName) values ('Liten Straight')
insert into ScoreNames(ScoreName) values ('Stor Straight')
insert into ScoreNames(ScoreName) values ('Kåk')
insert into ScoreNames(ScoreName) values ('Chans')
insert into ScoreNames(ScoreName) values ('Yatzy')
insert into ScoreNames(ScoreName) values ('Summa')

/****** StoredProcedure [dbo].[uspGetAllTurnsFromGameID] ******/
	SET ANSI_NULLS ON
	GO
	SET QUOTED_IDENTIFIER ON
	GO
CREATE PROCEDURE uspGetAllTurnsFromGameID
@gameid int
AS
SELECT 
	T.Turn as 'Omgång', 
	P.Name as 'Spelare', 
	SN.ScoreName as 'Rubrik', 
	T.TurnScore as 'Poäng', 
	DC.Die1 as 'Tärning 1', 
	DC.Die2 as 'Tärning 2', 
	DC.Die3 as 'Tärning 3', 
	DC.Die4 as 'Tärning 4', 
	DC.Die5 as 'Tärning 5'
FROM dbo.Turns as T
JOIN dbo.DiceCombo as DC
ON T.DiceCombo_ID = DC.DiceComboID
JOIN dbo.ScoreNames as SN
ON T.ScoreName_ID = SN.ScoreNameID
JOIN dbo.Players_Games as PG
ON T.PlayerGame_ID = PG.PlayerGameID
JOIN dbo.Players as P
ON PG.Player_ID = P.PlayerID
WHERE PG.Game_ID = @gameid
ORDER BY T.Turn, T.TurnID


/****** StoredProcedure [dbo].[uspInsertPlayer] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspInsertPlayer]
   @name VARCHAR(20) OUTPUT,
   @playerID INT OUTPUT
AS
SET XACT_ABORT ON
BEGIN
DECLARE @counter INT = 0
DECLARE @nameLength INT = LEN(@name)
	WHILE(EXISTS (SELECT Name FROM [dbo].[Players] WHERE Name = @name))
		BEGIN
			SET @counter = @counter + 1
			SET @name = SUBSTRING(@name, 1, @nameLength) + CAST(@counter AS VARCHAR(3))
		END
	
	INSERT [dbo].[Players] (Name) VALUES (@name)
	
	SELECT @playerID = PlayerID, @name = Name
	FROM [dbo].[Players]
	WHERE Name = @name
END


/****** StoredProcedure [dbo].[uspGetPlayerStats] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspGetPlayerStats]
   @playerID int
AS
SET XACT_ABORT ON
BEGIN
select PG.Game_ID, P.Name, PG.FinalScore, G.EndDate
from dbo.Players_Games AS PG
JOIN dbo.Players as P
ON PG.Player_ID = P.PlayerID
JOIN dbo.Games as G
ON PG.Game_ID = G.GameID
where PG.Game_ID IN (SELECT Game_ID 
						FROM dbo.Players_Games
						WHERE Player_ID = @playerID
						GROUP BY Game_ID)
ORDER BY Game_ID, FinalScore Desc
END

/****** StoredProcedure [dbo].[uspNewGame] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNewGame]
	@enddate date,
   @gameID INT OUTPUT
AS
SET XACT_ABORT ON
BEGIN
	IF @enddate IS NULL
		INSERT [dbo].[Games] DEFAULT VALUES 
	ELSE
		INSERT [dbo].[Games] (EndDate) VALUES (@enddate)
	
	SELECT TOP(1) @gameID = GameID 
	FROM [dbo].[Games]
	ORDER BY GameID DESC
END

/****** StoredProcedure [dbo].[uspNewPlayerGamesEntry] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspNewPlayerGamesEntry]
	@gameID INT,
	@playerID INT,
	@finalScore INT,
	@topHalfScore INT,
	@bonus INT,
   @playergameID INT OUTPUT
AS
SET XACT_ABORT ON
BEGIN
	INSERT INTO [dbo].[Players_Games] (Game_ID, Player_ID, FinalScore, TopHalfScore, Bonus) 
		VALUES (@gameID, @playerID, @finalScore, @topHalfScore, @bonus)
	
	SELECT TOP(1) @playergameID = PlayerGameID 
	FROM [dbo].[Players_Games]
	ORDER BY PlayerGameID DESC
END

/****** StoredProcedure [dbo].[uspInsertDiceCombo] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspInsertDiceCombo]
	@Die1 INT,
	@Die2 INT,
	@Die3 INT,
	@Die4 INT,
	@Die5 INT,
   @diceComboID INT OUTPUT
AS
SET XACT_ABORT ON
BEGIN
	INSERT INTO [dbo].[DiceCombo] (Die1, Die2, Die3, Die4, Die5) 
		VALUES (@Die1, @Die2, @Die3, @Die4,	@Die5)
	
	SELECT TOP(1) @diceComboID = DiceComboID 
	FROM [dbo].[DiceCombo]
	ORDER BY DiceComboID DESC
END


/****** StoredProcedure [dbo].[uspInsertTurn] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[uspInsertTurn]
	@turn INT,
	@scoreNameID INT,
	@turnScore INT,
	@diceComboID INT,
	@playerGameID INT
AS
SET XACT_ABORT ON
BEGIN
	INSERT INTO [dbo].[Turns] (Turn, ScoreName_ID, TurnScore, DiceCombo_ID, PlayerGame_ID) 
		VALUES (@turn, @scoreNameID, @turnScore, @diceComboID, @playerGameID)
END


/****** View [dbo].[vHighScore] ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW vHighScore
AS
SELECT TOP(10) P.Name, PG.FinalScore
FROM dbo.Players_Games as PG
JOIN dbo.Players as P
ON PG.Player_ID = P.PlayerID
ORDER BY PG.FinalScore Desc


